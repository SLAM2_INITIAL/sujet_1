﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sujet_1

    /* Les classes métier et outils sont déjà en place vous rajouterez les éventuelles using manquants
     * 
     * 
     * 1. Placer un bouton sur la feuille (form) dont le nom sera btnAuth et le Text Authentification
     * 2. Cliquez sur le bouton en mode développement afin de créer une procédure sur l'événement click du bouton
     * 3. Réalisez sur une feuille de papier qui comporetra votre nom , le diagramme de la séquence de code décrite à la question 4.
     * La granularité de ce diagramme sera assez faible (Niveau de détail assez élevé) aussi vous ferez apparaitre les type collection nécessaire à sa réalisation.
     * 4. Réalisez le code pour que le comportement du programme soit le suivant : après avoir saisi son username (login) et son password 
     * l'action sur le bouton de commande déclenchera une recherche à l'aide de la méthode fonction verificationCnx dont 
     * la structure (signature) est déja présente dans la classe de contrôle.
     * Deux cas vont se présenter :
     *  cas n°1 : en cas de recherche infructueuse le programme affiche un message dans une fenêtre prévue à cet effet (messageBox)
     *  message : Désolé votre Login ou votre mot de passe est incorrect.
     *  cas N°2 : si la recherche est couronnée de succès le message indiquera : bienvenue M. "Nom Prénom" deumeurant "Adresse". Cet affichage sera 
     *  de la responsabilité de la classe de d'affichage
     *  
     *  Remarques : les commentaires expliquant ce que vous faites dans le code seront valorisés - Par exemple instanciation 
     *  boucle sur telle collection etc ...
    */
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }
    }
}
